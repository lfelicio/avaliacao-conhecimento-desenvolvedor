package com.empresa.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.empresa.model.Colaborador;
import com.empresa.model.Gerente;
import com.empresa.service.ColaboradorService;

@ExtendWith(MockitoExtension.class)
public class ColaboradorControllerTest {

	@Mock
	private ColaboradorService colaboradorService;

	@InjectMocks
	private ColaboradorController colaboradorController;

	private Page<Colaborador> colaboradoresPage;

	private Colaborador colaborador;

	private Gerente gerente;

	@BeforeEach
	public void setup() {
		// Inicialize seus dados de teste aqui
		colaboradoresPage = new PageImpl<>(createColaboradoresList());
		colaborador = new Colaborador(1L, "12345678900", "Fulano", LocalDate.now(), "Analista",
				BigDecimal.valueOf(5000), null);
		gerente = new Gerente(colaborador);
	}

	@Test
	public void testListarColaboradores() {
		PageRequest pageable = PageRequest.of(0, 10);
		when(colaboradorService.listarColaboradores(pageable)).thenReturn(colaboradoresPage);

		ResponseEntity<Page<Colaborador>> response = colaboradorController.listarColaboradores(pageable);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(colaboradoresPage, response.getBody());
	}

	@Test
	public void testBuscarColaboradorPorId_Existente() {
		Long id = 1L;
		when(colaboradorService.buscarColaboradorPorId(id)).thenReturn(colaborador);

		ResponseEntity<Colaborador> response = colaboradorController.buscarColaboradorPorId(id);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(colaborador, response.getBody());
	}

	@Test
	public void testBuscarColaboradorPorId_NaoExistente() {
		Long id = 1L;
		when(colaboradorService.buscarColaboradorPorId(id)).thenReturn(null);

		ResponseEntity<Colaborador> response = colaboradorController.buscarColaboradorPorId(id);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void testBuscarColaboradorPorCpf_Existente() {
		String cpf = "123.456.789-00";
		when(colaboradorService.buscarColaboradorPorCpf(cpf)).thenReturn(colaborador);

		ResponseEntity<Colaborador> response = colaboradorController.buscarColaboradorPorCpf(cpf);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(colaborador, response.getBody());
	}

	@Test
	public void testBuscarColaboradorPorCpf_NaoExistente() {
		String cpfNaoExistente = "999.999.999-99";
		when(colaboradorService.buscarColaboradorPorCpf(cpfNaoExistente)).thenReturn(null);


		ResponseEntity<Colaborador> response = colaboradorController.buscarColaboradorPorCpf(cpfNaoExistente);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void testBuscarColaboradorPorAno_Existente() {
		int ano = LocalDate.now().getYear();
		PageRequest pageable = PageRequest.of(0, 10);
		when(colaboradorService.buscarColaboradorPorAno(ano, pageable)).thenReturn(colaboradoresPage);

		ResponseEntity<Page<Colaborador>> response = colaboradorController.buscarColaboradorPorAno(ano, pageable);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(colaboradoresPage, response.getBody());
	}

	@Test
	public void testBuscarColaboradorPorAno_NaoExistente() {
		int ano = 2020;
		PageRequest pageable = PageRequest.of(0, 10);
		when(colaboradorService.buscarColaboradorPorAno(ano, pageable)).thenReturn(Page.empty());

		ResponseEntity<Page<Colaborador>> response = colaboradorController.buscarColaboradorPorAno(ano, pageable);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void testListarSubordinadosPorNome() {
		String nome = "Gerente";
		PageRequest pageableGerente = PageRequest.of(0, 10);
		PageRequest pageableSubordinados = PageRequest.of(0, 10);
		when(colaboradorService.listarSubordinadosPorNome(nome, pageableGerente, pageableSubordinados))
				.thenReturn(Page.empty());

		ResponseEntity<Page<Gerente>> response = colaboradorController.listarSubordinadosPorNome(nome, 0, 10, 0, 10);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void testCriarColaborador() {
		when(colaboradorService.adicionarColaborador(colaborador)).thenReturn(colaborador);

		ResponseEntity<Colaborador> response = colaboradorController.criarColaborador(colaborador);

		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertEquals(colaborador, response.getBody());
	}

	@Test
	public void testAtualizarColaborador_Existente() {
		Long id = 1L;
		when(colaboradorService.atualizarColaborador(id, colaborador)).thenReturn(colaborador);

		ResponseEntity<Colaborador> response = colaboradorController.atualizarColaborador(id, colaborador);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(colaborador, response.getBody());
	}

	@Test
	public void testAtualizarColaborador_NaoExistente() {
		Long id = 1L;
		when(colaboradorService.atualizarColaborador(id, colaborador)).thenReturn(null);

		ResponseEntity<Colaborador> response = colaboradorController.atualizarColaborador(id, colaborador);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void testExcluirColaborador() {
		Long id = 1L;
		doNothing().when(colaboradorService).excluirColaborador(id);

		ResponseEntity<String> response = colaboradorController.excluirColaborador(id);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals("Colaborador excluído com sucesso", response.getBody());
	}


	@Test
	public void testEncontrarGerenteResponsavel_Existente() {
		Long idColaboradorA = 1L;
		Long idColaboradorB = 2L;
		when(colaboradorService.encontrarGerenteResponsavel(idColaboradorA, idColaboradorB)).thenReturn(gerente);
		ResponseEntity<Colaborador> response = colaboradorController.encontrarGerenteResponsavel(idColaboradorA,
				idColaboradorB);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(gerente, response.getBody());
	}

	@Test
	void testEncontrarGerenteResponsavel_NaoEncontrado() {
		Long idColaboradorA = 1L;
		Long idColaboradorB = 2L;
		when(colaboradorService.encontrarGerenteResponsavel(anyLong(), anyLong())).thenReturn(null);

		ResponseEntity<Colaborador> response = colaboradorController.encontrarGerenteResponsavel(idColaboradorA,
				idColaboradorB);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
		assertEquals(null, response.getBody());
	}

	private List<Colaborador> createColaboradoresList() {
		List<Colaborador> colaboradores = new ArrayList<>();
		colaboradores.add(new Colaborador(1L, "12345678900", "Fulano", LocalDate.now(), "Analista",
				BigDecimal.valueOf(5000), null));
		colaboradores.add(new Colaborador(2L, "98765432100", "Beltrano", LocalDate.now(), "Gerente",
				BigDecimal.valueOf(7000), 1L));
		return colaboradores;
	}

}
