package com.empresa.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.br.CPF;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

/**
 * Representa um colaborador da empresa.
 */
@Entity
@Table(name = "colaborador")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Colaborador {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String cpf;

	private String nome;

	@Column(name = "data_admissao")
	@JsonFormat(pattern = "dd/MM/yyyy")
	private LocalDate dataAdmissao;

	private String funcao;

	private BigDecimal remuneracao;

	@Column(name = "gerente_id")
	private Long gerenteId;

	/**
	 * Executado antes da persistência do colaborador. Este método formata o CPF e a
	 * remuneração antes de persistir no banco de dados.
	 */
	@PrePersist
	public void prePersist() {
		if (this.cpf != null) {
			String cpfNumerico = this.cpf.replaceAll("[^0-9]", "");

			this.cpf = String.format("%s.%s.%s-%s", cpfNumerico.substring(0, 3), cpfNumerico.substring(3, 6),
					cpfNumerico.substring(6, 9), cpfNumerico.substring(9));
		}
		if (this.remuneracao != null) {
			this.remuneracao = this.remuneracao.setScale(2, RoundingMode.HALF_UP);
		}
	}

}
