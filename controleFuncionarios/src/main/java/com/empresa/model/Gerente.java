package com.empresa.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Representa um Gerente, que é um tipo específico de Colaborador com subordinados.
 */
@Getter
@Setter
public class Gerente extends Colaborador {

	/** Lista de subordinados do gerente. */
	private List<Colaborador> subordinados;

	/**
	 * Construtor que cria um objeto Gerente a partir de um Colaborador.
	 * @param colaborador O colaborador a partir do qual o Gerente será criado.
	 */
	public Gerente(Colaborador colaborador) {
		this.setId(colaborador.getId());
		this.setCpf(colaborador.getCpf());
		this.setNome(colaborador.getNome());
		this.setDataAdmissao(colaborador.getDataAdmissao());
		this.setFuncao(colaborador.getFuncao());
		this.setRemuneracao(colaborador.getRemuneracao());
		this.setGerenteId(colaborador.getGerenteId());
		this.subordinados = new ArrayList<>();
	}

}
