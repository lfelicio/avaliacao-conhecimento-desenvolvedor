package com.empresa.controller;

import com.empresa.model.Colaborador;
import com.empresa.model.Gerente;
import com.empresa.service.ColaboradorService;
import com.empresa.exception.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import jakarta.validation.Valid;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Controlador REST para manipulação de colaboradores.
 */
@RestController
@RequestMapping("/colaboradores")
public class ColaboradorController {

	private final ColaboradorService colaboradorService;

	private static final Logger logger = LoggerFactory.getLogger(ColaboradorController.class);

	public static final String EMPTY_JSON = ": {}";

	public static final String ERRO_DIGITOS_CPF = "CPF deve conter 11 dígitos";

	public static final String ERRO_COLABORADORES = "Colaboradores não encontrados";

	public static final int TAMANHO_CPF = 11;

	public static final String ERRO_FORMATO_DATA = "Formato de data inválido. Use o formato dd/MM/yyyy";

	public static final String FORMATO_DATA = "dd/MM/yyyy";

	public static final String REGEX_APENAS_DIGITOS = "[^0-9]";

	public static final String STRING_VAZIA = "";

	public static final String DEFAULT_PAGE = "0";

	public static final String DEFAULT_SIZE = "10";

	public static final String COLABORADOR_EXCLUIDO = "Colaborador excluído com sucesso";

	private static final String MASCARA_CPF = "$1.$2.$3-$4";

	private static final String FORMATO_CPF = "(\\d{3})(\\d{3})(\\d{3})(\\d{2})";


	/**
	 * Construtor para o controlador de colaboradores.
	 * @param colaboradorService O serviço de colaborador a ser injetado
	 */
	@Autowired
	public ColaboradorController(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}

	@GetMapping
	public ResponseEntity<Page<Colaborador>> listarColaboradores(Pageable pageable) {
		try {
			Page<Colaborador> listaColaaboradores = colaboradorService.listarColaboradores(pageable);
			if (!listaColaaboradores.isEmpty())
				return ResponseEntity.ok(listaColaaboradores);
			else
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ERRO_COLABORADORES);
		}
		catch (Exception e) {
			logger.error(ErrorMessage.ERRO_LISTAR_COLABORADORES + EMPTY_JSON, e.getMessage());
			ErrorMessage.throwInternalServerError(ErrorMessage.ERRO_LISTAR_COLABORADORES, e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
	}

	/**
	 * Busca um colaborador pelo ID.
	 * @param id O ID do colaborador a ser buscado
	 * @return O colaborador encontrado ou uma resposta 404 se não encontrado
	 */
	@GetMapping("/{id}")
	public ResponseEntity<Colaborador> buscarColaboradorPorId(@PathVariable Long id) {
		try {
			Colaborador colaborador = colaboradorService.buscarColaboradorPorId(id);
			if (colaborador != null) {
				return ResponseEntity.ok(colaborador);
			}
			else {
				return ResponseEntity.notFound().build();
			}
		}
		catch (Exception e) {
			logger.error(ErrorMessage.ERRO_BUSCAR_COLABORADOR_ID + EMPTY_JSON, e.getMessage());
			ErrorMessage.throwInternalServerError(ErrorMessage.ERRO_BUSCAR_COLABORADOR_ID, e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
	}

	/**
	 * Busca um colaborador pelo CPF.
	 * @param cpf O CPF do colaborador a ser buscado
	 * @return O colaborador encontrado ou uma resposta 404 se não encontrado
	 */
	@GetMapping("/cpf/{cpf}")
	public ResponseEntity<Colaborador> buscarColaboradorPorCpf(@PathVariable String cpf) {
		try {
			Colaborador colaborador = colaboradorService.buscarColaboradorPorCpf(formatarCPF(cpf));
			if (colaborador != null) {
				return ResponseEntity.ok(colaborador);
			}
			else {
				return ResponseEntity.notFound().build();
			}
		}
		catch (Exception e) {
			logger.error(ErrorMessage.ERRO_BUSCAR_COLABORADOR_CPF + EMPTY_JSON, e.getMessage());
			ErrorMessage.throwInternalServerError(ErrorMessage.ERRO_BUSCAR_COLABORADOR_CPF, e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);

		}
	}

	/**
	 * Busca uma lista de colaboradores pelo ano de admissão.
	 * @param ano O ano de admissão a ser buscado
	 * @param pageable dados da paginação
	 * @return uma Page com os colaboraes do ano buscado
	 */
	@GetMapping("/ano/{ano}")
	public ResponseEntity<Page<Colaborador>> buscarColaboradorPorAno(@PathVariable int ano, Pageable pageable) {
		try {
			Page<Colaborador> colaboradores = colaboradorService.buscarColaboradorPorAno(ano, pageable);
			if (!colaboradores.isEmpty()) {
				return ResponseEntity.ok(colaboradores);
			}
			else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
			}
		}
		catch (Exception e) {
			logger.error(ErrorMessage.ERRO_BUSCAR_COLABORADOR_ANO + EMPTY_JSON, e.getMessage());
			ErrorMessage.throwInternalServerError(ErrorMessage.ERRO_BUSCAR_COLABORADOR_ANO, e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);

		}
	}

	/**
	 * Busca uma lista de colaboradores e seus subordinados por nome dos colaboradores.
	 * @param nome O nome a ser buscado
	 * @param 'page' e 'size', dados da paginação
	 * @return uma Page com os colaboraes e subordinados buscados
	 */
	@GetMapping("/subordinados/{nome}")
	public ResponseEntity<Page<Gerente>> listarSubordinadosPorNome(@PathVariable String nome,
			@RequestParam(defaultValue = DEFAULT_PAGE) int pageGerente,
			@RequestParam(defaultValue = DEFAULT_SIZE) int sizeGerente,
			@RequestParam(defaultValue = DEFAULT_PAGE) int pageSubordinados,
			@RequestParam(defaultValue = DEFAULT_SIZE) int sizeSubordinados) {
		try {
			Pageable pageableGerente = PageRequest.of(pageGerente, sizeGerente);
			Pageable pageableSubordinados = PageRequest.of(pageSubordinados, sizeSubordinados);

			Page<Gerente> gerentesComSubordinadosPage = colaboradorService.listarSubordinadosPorNome(nome,
					pageableGerente, pageableSubordinados);
			if (!gerentesComSubordinadosPage.isEmpty())
				return ResponseEntity.ok(gerentesComSubordinadosPage);
			else
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		catch (Exception e) {
			logger.error(ErrorMessage.ERRO_LISTAR_SUBORDINADOS + EMPTY_JSON, e.getMessage());
			ErrorMessage.throwInternalServerError(ErrorMessage.ERRO_LISTAR_SUBORDINADOS, e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);

		}
	}

	/**
	 * cria um novo colaborador.
	 * @param colaborador o colaborador a ser inserido
	 * @return o colaborador inserido com seu id
	 */
	@PostMapping
	public ResponseEntity<Colaborador> criarColaborador(@Valid @RequestBody Colaborador colaborador) {
		try {
			validarCPF(colaborador.getCpf());
			validarData(colaborador.getDataAdmissao());

			Colaborador novoColaborador = colaboradorService.adicionarColaborador(colaborador);
			return ResponseEntity.status(HttpStatus.CREATED).body(novoColaborador);
		}
		catch (Exception e) {
			logger.error(ErrorMessage.ERRO_CRIAR_COLABORADOR + EMPTY_JSON, e.getMessage());
			ErrorMessage.throwInternalServerError(ErrorMessage.ERRO_CRIAR_COLABORADOR, e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);

		}
	}

	/**
	 * atualiza colaborador atraves do seu id.
	 * @param id O id do colaborador a ser alterado
	 * @param colaborador os dados alterados do colaborador
	 * @return o colaborador com as alterações
	 */
	@PutMapping("/{id}")
	public ResponseEntity<Colaborador> atualizarColaborador(@PathVariable Long id,
			@RequestBody Colaborador colaborador) {
		try {
			if (colaborador.getCpf() != null) {
				validarCPF(colaborador.getCpf());
			}

			if (colaborador.getDataAdmissao() != null) {
				validarData(colaborador.getDataAdmissao());
			}

			Colaborador colaboradorAtualizado = colaboradorService.atualizarColaborador(id, colaborador);
			if (colaboradorAtualizado != null) {
				return ResponseEntity.ok(colaboradorAtualizado);
			}
			else {
				return ResponseEntity.notFound().build();
			}
		}
		catch (Exception e) {
			logger.error(ErrorMessage.ERRO_ATUALIZAR_COLABORADOR + EMPTY_JSON, e.getMessage());
			ErrorMessage.throwInternalServerError(ErrorMessage.ERRO_ATUALIZAR_COLABORADOR, e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);

		}
	}

	/**
	 * exclui um colaborador
	 * @param id o id do colaborador a ser deletado
	 */
	@DeleteMapping("/{id}")
	public ResponseEntity<String> excluirColaborador(@PathVariable Long id) {
		try {
			colaboradorService.excluirColaborador(id);
			return ResponseEntity.ok(COLABORADOR_EXCLUIDO);
		}
		catch (Exception e) {
			logger.error(ErrorMessage.ERRO_EXCLUIR_COLABORADOR + EMPTY_JSON, e.getMessage());
			ErrorMessage.throwInternalServerError(ErrorMessage.ERRO_EXCLUIR_COLABORADOR, e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);

		}
	}

	/**
	 * encontra o gerente comum a dois colaboradores.
	 * @param idColaboradorA id do colaborador a
	 * @param idColaboradorB id do colaborador b
	 * @return o colaborador que é gerente comum
	 */
	@GetMapping("/gerente-responsavel/{idColaboradorA}/{idColaboradorB}")
	public ResponseEntity<Colaborador> encontrarGerenteResponsavel(@PathVariable Long idColaboradorA,
			@PathVariable Long idColaboradorB) {
		try {
			Colaborador gerenteResponsavel = colaboradorService.encontrarGerenteResponsavel(idColaboradorA,
					idColaboradorB);
			if (gerenteResponsavel != null)
				return ResponseEntity.ok(gerenteResponsavel);
			else
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
		catch (Exception e) {
			logger.error(ErrorMessage.ERRO_ENCONTRAR_GERENTE_RESPONSAVEL + EMPTY_JSON, e.getMessage());
			ErrorMessage.throwInternalServerError(ErrorMessage.ERRO_ENCONTRAR_GERENTE_RESPONSAVEL, e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);

		}
	}

	/**
	 * Valida o CPF do colaborador.
	 * @param cpf o CPF a ser validado
	 */
	private void validarCPF(String cpf) {
		if (cpf.replaceAll(REGEX_APENAS_DIGITOS, STRING_VAZIA).length() != TAMANHO_CPF) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ERRO_DIGITOS_CPF);
		}
	}

	/**
	 * Valida a data de admissão do colaborador.
	 * @param data a data de admissão a ser validada
	 */
	private void validarData(LocalDate data) {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMATO_DATA);
			String dataFormatada = data.format(formatter);
			LocalDate.parse(dataFormatada, formatter);
		}
		catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ERRO_FORMATO_DATA, e);
		}
	}

	/**
	 * Formata o CPF adicionando a máscara se não estiver presente.
	 * @param cpf O CPF a ser formatado.
	 * @return O CPF formatado.
	 */
	public static String formatarCPF(String cpf) {

		cpf = cpf.replaceAll(REGEX_APENAS_DIGITOS, STRING_VAZIA);

		// Adiciona a máscara ao CPF
		return cpf.replaceAll(FORMATO_CPF, MASCARA_CPF);
	}

}
