package com.empresa.service;

import com.empresa.model.Colaborador;
import com.empresa.model.Gerente;
import com.empresa.repository.ColaboradorRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;

import java.math.RoundingMode;
import java.util.*;

/**
 * Serviço responsável pela lógica de negócios relacionada aos colaboradores.
 */
@Service
public class ColaboradorService {

	private final ColaboradorRepository colaboradorRepository;

	private static final String COLABORADOR_NAO_ENCONTRADO = "Colaborador não encontrado";

	private static final String GERENTE_NAO_ENCONTRADO = "Gerente não encontrado";

	private static final String GERENTE_COMUM_NAO_ENCONTRADO = "Nenhum gerente comum encontrado";

	private static final String CPF_JA_CADASTRADO = "CPF já cadastrado";

	private static final String PAGINA_INVALIDA = "Número de página inválido. O total de páginas é ";

	/**
	 * Construtor da classe.
	 * @param colaboradorRepository O repositório de colaboradores.
	 */
	public ColaboradorService(ColaboradorRepository colaboradorRepository) {
		this.colaboradorRepository = colaboradorRepository;
	}

	/**
	 * Lista todos os colaboradores paginados.
	 * @param pageable As informações de paginação.
	 * @return Uma página contendo os colaboradores.
	 */
	public Page<Colaborador> listarColaboradores(Pageable pageable) {
		Page<Colaborador> page = colaboradorRepository.findAll(pageable);
		validarPagina(page);
		return page;
	}

	/**
	 * Busca um colaborador pelo ID.
	 * @param id O ID do colaborador a ser buscado.
	 * @return O colaborador encontrado ou null se não encontrado.
	 */
	public Colaborador buscarColaboradorPorId(Long id) {
		return colaboradorRepository.findById(id).orElse(null);
	}

	/**
	 * Busca um colaborador pelo CPF.
	 * @param cpf O CPF do colaborador a ser buscado.
	 * @return O colaborador encontrado ou null se não encontrado.
	 */
	public Colaborador buscarColaboradorPorCpf(String cpf) {
		return colaboradorRepository.findByCpf(cpf).orElse(null);
	}

	/**
	 * Busca os colaboradores admitidos em um determinado ano, paginados.
	 * @param ano O ano de admissão dos colaboradores.
	 * @param pageable As informações de paginação.
	 * @return Uma página contendo os colaboradores admitidos no ano especificado.
	 */
	public Page<Colaborador> buscarColaboradorPorAno(int ano, Pageable pageable) {
		Page<Colaborador> page = colaboradorRepository.findByAnoDataAdmissao(ano, pageable);
		validarPagina(page);
		return page;
	}

	/**
	 * Lista os subordinados de todos os gerentes cujo nome contenha a sequência
	 * fornecida, paginados.
	 * @param nome O nome a ser pesquisado nos gerentes.
	 * @param pageableGerente As informações de paginação para os gerentes.
	 * @param pageableSubordinados As informações de paginação para os subordinados dos
	 * gerentes.
	 * @return Uma página contendo os gerentes com seus subordinados.
	 */
	public Page<Gerente> listarSubordinadosPorNome(String nome, Pageable pageableGerente,
			Pageable pageableSubordinados) {
		Page<Colaborador> colaboradoresPage = colaboradorRepository.findByNomeContainingIgnoreCase(nome,
				pageableGerente);
		Page<Gerente> gerentesComSubordinadosPage = colaboradoresPage.map(colaborador -> {
			Page<Colaborador> subordinadosPage = colaboradorRepository.findByGerenteId(colaborador.getId(),
					pageableSubordinados);
			validarPagina(subordinadosPage);
			Gerente gerente = new Gerente(colaborador);
			gerente.setSubordinados(subordinadosPage.getContent());
			return gerente;
		});
		validarPagina(gerentesComSubordinadosPage);
		return gerentesComSubordinadosPage;
	}

	/**
	 * Adiciona um novo colaborador.
	 * @param colaborador O colaborador a ser adicionado.
	 * @return O colaborador adicionado.
	 */
	public Colaborador adicionarColaborador(Colaborador colaborador) {
		String cpf = colaborador.getCpf();
		if (colaboradorRepository.findByCpf(cpf).isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, CPF_JA_CADASTRADO);
		}
		return colaboradorRepository.save(colaborador);
	}

	/**
	 * Atualiza um colaborador existente.
	 * @param id O ID do colaborador a ser atualizado.
	 * @param colaboradorAtualizado O objeto com os novos dados do colaborador.
	 * @return O colaborador atualizado.
	 */
	public Colaborador atualizarColaborador(Long id, Colaborador colaboradorAtualizado) {
		Colaborador colaboradorExistente = colaboradorRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, COLABORADOR_NAO_ENCONTRADO));

		if (colaboradorAtualizado.getNome() != null) {
			colaboradorExistente.setNome(colaboradorAtualizado.getNome());
		}
		if (colaboradorAtualizado.getDataAdmissao() != null) {
			colaboradorExistente.setDataAdmissao(colaboradorAtualizado.getDataAdmissao());
		}
		if (colaboradorAtualizado.getFuncao() != null) {
			colaboradorExistente.setFuncao(colaboradorAtualizado.getFuncao());
		}
		if (colaboradorAtualizado.getRemuneracao() != null) {
			colaboradorExistente
					.setRemuneracao(colaboradorAtualizado.getRemuneracao().setScale(2, RoundingMode.HALF_UP));
		}
		if (colaboradorAtualizado.getGerenteId() != null) {
			colaboradorExistente.setGerenteId(colaboradorAtualizado.getGerenteId());
		}

		return colaboradorRepository.save(colaboradorExistente);
	}

	/**
	 * Exclui um colaborador pelo ID.
	 * @param id O ID do colaborador a ser excluído.
	 */
	public void excluirColaborador(Long id) {
		colaboradorRepository.deleteById(id);
	}

	/**
	 * Valida se a página solicitada é válida.
	 * @param page A página a ser validada.
	 */
	private void validarPagina(Page<?> page) {
		if (page.getNumber() >= page.getTotalPages()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, PAGINA_INVALIDA + page.getTotalPages());
		}
	}

	/**
	 * Encontra o gerente responsável comum entre dois colaboradores.
	 * @param idColaboradorA O ID do colaborador A.
	 * @param idColaboradorB O ID do colaborador B.
	 * @return O gerente responsável comum.
	 * @throws ResponseStatusException Se algum dos colaboradores ou gerentes não for
	 * encontrado.
	 */
	public Colaborador encontrarGerenteResponsavel(Long idColaboradorA, Long idColaboradorB) {
		try {
			Colaborador colaboradorA = colaboradorRepository.findById(idColaboradorA)
					.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, COLABORADOR_NAO_ENCONTRADO));

			Colaborador colaboradorB = colaboradorRepository.findById(idColaboradorB)
					.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, COLABORADOR_NAO_ENCONTRADO));

			List<Long> hierarquiaGerentesA = gerarHierarquiaGerentes(colaboradorA);
			List<Long> hierarquiaGerentesB = gerarHierarquiaGerentes(colaboradorB);

			for (Long gerenteId : hierarquiaGerentesA) {
				if (hierarquiaGerentesB.contains(gerenteId)) {
					return colaboradorRepository.findById(gerenteId).orElseThrow(
							() -> new ResponseStatusException(HttpStatus.NOT_FOUND, GERENTE_NAO_ENCONTRADO));
				}
			}

			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, GERENTE_COMUM_NAO_ENCONTRADO);
		}
		catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, GERENTE_COMUM_NAO_ENCONTRADO, e);
		}
	}

	/**
	 * Gera a hierarquia de gerentes para um colaborador.
	 * @param colaborador O colaborador para o qual a hierarquia de gerentes será gerada.
	 * @return A lista de IDs dos gerentes na hierarquia.
	 * @throws ResponseStatusException Se algum dos gerentes não for encontrado.
	 */
	private List<Long> gerarHierarquiaGerentes(Colaborador colaborador) {
		List<Long> hierarquiaGerentes = new ArrayList<>();
		Long gerenteId = colaborador.getGerenteId();

		while (gerenteId != null) {
			hierarquiaGerentes.add(gerenteId);
			Colaborador gerente = colaboradorRepository.findById(gerenteId)
					.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Gerente não encontrado"));
			gerenteId = gerente.getGerenteId();
		}

		return hierarquiaGerentes;
	}

}
