package com.empresa.repository;

import com.empresa.model.Colaborador;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repositório para a entidade Colaborador.
 */
@Repository
public interface ColaboradorRepository extends JpaRepository<Colaborador, Long> {

	/**
	 * Retorna uma página de todos os colaboradores.
	 * @param pageable Objeto contendo informações sobre a paginação
	 * @return Página de colaboradores
	 */
	Page<Colaborador> findAll(Pageable pageable);

	/**
	 * Busca um colaborador pelo CPF.
	 * @param cpf CPF do colaborador a ser buscado
	 * @return O colaborador encontrado, caso exista
	 */
	Optional<Colaborador> findByCpf(String cpf);

	/**
	 * Busca colaboradores admitidos em um determinado ano.
	 * @param ano Ano de admissão dos colaboradores a serem buscados
	 * @param pageable Objeto contendo informações sobre a paginação
	 * @return Página de colaboradores admitidos no ano especificado
	 */
	@Query("SELECT c FROM Colaborador c WHERE EXTRACT(YEAR FROM c.dataAdmissao) = :ano")
	Page<Colaborador> findByAnoDataAdmissao(@Param("ano") int ano, Pageable pageable);

	/**
	 * Busca colaboradores pelo nome, ignorando diferenças de maiúsculas e minúsculas.
	 * @param nome Nome ou parte do nome do colaborador a ser buscado
	 * @param pageable Objeto contendo informações sobre a paginação
	 * @return Página de colaboradores cujo nome contenha a string fornecida
	 */
	Page<Colaborador> findByNomeContainingIgnoreCase(String nome, Pageable pageable);

	/**
	 * Busca colaboradores pelo ID do gerente, retornando uma página.
	 * @param gerenteId ID do gerente dos colaboradores a serem buscados
	 * @param pageable Objeto contendo informações sobre a paginação
	 * @return Página de colaboradores do gerente especificado
	 */
	Page<Colaborador> findByGerenteId(Long gerenteId, Pageable pageable);

}
