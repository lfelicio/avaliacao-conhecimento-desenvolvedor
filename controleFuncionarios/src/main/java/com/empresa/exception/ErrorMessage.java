package com.empresa.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ErrorMessage {

	// Mensagens de erro para o controller
	public static final String ERRO_LISTAR_COLABORADORES = "Erro ao listar colaboradores";

	public static final String ERRO_BUSCAR_COLABORADOR_ID = "Erro ao buscar colaborador por ID";

	public static final String ERRO_BUSCAR_COLABORADOR_CPF = "Erro ao buscar colaborador por CPF";

	public static final String ERRO_BUSCAR_COLABORADOR_ANO = "Erro ao buscar colaboradores por ano";

	public static final String ERRO_LISTAR_SUBORDINADOS = "Erro ao listar subordinados por nome";

	public static final String ERRO_CRIAR_COLABORADOR = "Erro ao criar colaborador";

	public static final String ERRO_ATUALIZAR_COLABORADOR = "Erro ao atualizar colaborador";

	public static final String ERRO_EXCLUIR_COLABORADOR = "Erro ao excluir colaborador";

	public static final String ERRO_ENCONTRAR_GERENTE_RESPONSAVEL = "Erro ao encontrar gerente responsável";

	public static void throwInternalServerError(String message, Exception e) {
		throw new ResponseStatusException(HttpStatus.BAD_REQUEST, message, e);
	}

}
