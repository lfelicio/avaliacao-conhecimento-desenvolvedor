# Controle de Funcionários

Este é um projeto de controle de funcionários desenvolvido em Java com Spring Boot.

## Descrição

O objetivo deste projeto é fornecer uma aplicação para gerenciar colaboradores de uma empresa, permitindo listar, adicionar, atualizar e excluir colaboradores, bem como buscar colaboradores por diferentes critérios.

## Funcionalidades

- Listar todos os colaboradores paginados
- Buscar colaborador por ID
- Buscar colaborador por CPF
- Buscar colaboradores admitidos em um determinado ano
- Listar subordinados de gerentes por nome
- Criar um novo colaborador
- Atualizar um colaborador existente
- Excluir um colaborador
- Encontrar o gerente responsável comum entre dois colaboradores

## Tecnologias Utilizadas

- Java
- Spring Boot
- Spring Data JPA
- PostgreSQL
- Lombok

## Testes

Para garantir a qualidade do código, foram utilizadas as seguintes tecnologias de teste:

- Mockito
- JUnit

## Como Executar

1. Certifique-se de ter o JDK 17 e o Maven instalados.
2. Clone o repositório para sua máquina local.
3. Certifique-se de ter o JDK 17 e o Maven instalados.
4. Configure um banco de dados PostgreSQL e ajuste as configurações no arquivo `application.properties`.
5. Execute o comando `mvn spring-boot:run` na raiz do projeto para iniciar a aplicação.
6. Acesse a aplicação através do navegador ou de uma ferramenta de API como o Postman.

## Executando Testes

Para executar os testes, siga estas etapas:

1. Execute o comando `mvn test` na raiz do projeto.